// insert movie
db.movies.insertOne({
    "title" : "Avenger Endgame (2019)",
    "release_year" : 2019,
    "genres" : [
            "Fantasy",
            "Sci-Fi"
    ]
});
db.movies.insertMany([
    {"title" : "Glass", "release_year" : 2019, "genres" : [ "Drama", "Sci-Fi", "Thriller" ] },
    {"title" : "Fighting with My Family", "release_year" : 2019, "genres" : [ "Biography", "Comedy", "Drama" ] },
    {"title" : "Black Panther", "release_year" : 2018, "genres" : [ "Action", "Adventure", "Sci-Fi" ] }])
   
db.movies.find({'title' : "Black Panther"}, {})
db.movies.update({'title' : "Black Panther"}, {$set : {'release_year' : 2019}})
db.movies.delete({})
db.movie.find().sort({"_id": -1}).limit(1)
db.users.find({'age_id' : 1}).explain("executionStats")

db.users.createIndex({'age_id' : 1})

//fetching a frequency of genre of movies per year for all years
var mapFn = function(){
	var genres = this.genres;
	var year = this.release_year;
	//emit each genre as count 1
	for (var i = 0; i < genres.length; i++)
		emit({'year': year, 'genre': genres[i]}, 1);
}

var reduceFn = function(key, values) {
	return Array.sum(values);
}

db.movies.mapReduce(mapFn, reduceFn, {out: {inline: 1}, query: {}}) 

db.movies.mapReduce(mapFn, reduceFn, {out: 'genre_yearly_hist', query: {}}) 
show collections;

//aggregate function
db.movies.aggregate([
	{$project : {'release_year' : 1, 'genres' : 1, '_id' : 0}},
	{$unwind : '$genres'},
	{$group : {
		_id : { 'year' : '$release_year', 'genre' : '$genres'},
		value : {$sum : 1}
	}},
])

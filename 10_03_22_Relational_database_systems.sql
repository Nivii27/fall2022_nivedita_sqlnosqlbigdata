CREATE TABLE Account (
Account_id VARCHAR(10) not null,
Email VARCHAR(25) not null,
Website VARCHAR(20) not null,
Pronoun_id CHAR(4) not null,
primary KEY(Account_id));
update ACCOUNT set WEBSITE VARCHAR(100)
alter table ACCOUNT alter column WEBSITE type VARCHAR(100);
alter table ACCOUNT alter column email type VARCHAR(50);

CREATE TABLE Business_Account (
Account_id VARCHAR(10) not null,
Business_name VARCHAR(25) not null,
address_line_1 VARCHAR(20) not null,
city VARCHAR(4) not null,
State VARCHAR(6) not NULL,
Zip INTEGER not null,
Country VARCHAR(15) not null,
ABOUT CHAR(100),
Phone_number integer not null,
primary key(account_id),
CONSTRAINT Business_account_fkey_account FOREIGN key (account_id) REFERENCES Account(Account_id));
alter table BUSINESS_ACCOUNT alter column city type VARCHAR(50);
alter table BUSINESS_ACCOUNT alter column city type VARCHAR(50);
alter table BUSINESS_ACCOUNT alter column Business_name type VARCHAR(50);
alter table BUSINESS_ACCOUNT alter column about type VARCHAR(500);

drop table Personal_Account;
CREATE TABLE Personal_Account (
Personal_user_id VARCHAR(10) not null,
Username VARCHAR(25) not null,
First_name VARCHAR(20) not null,
Last_name VARCHAR(20) not null,
Short_bio CHAR(100),
primary key(Personal_user_id),
CONSTRAINT Personal_account_fkey_account FOREIGN key (Personal_user_id) REFERENCES Account(Account_id));






insert into ACCOUNT (ACCOUNT_ID, email, website, PRONOUN_ID) values (1003, 9, '/libero/convallis.jpg', 1);
insert into ACCOUNT (ACCOUNT_ID, email, website, PRONOUN_ID)  values (1005, 10, '/cubilia/curae/nulla/dapibus/dolor.json', 2);
select * from ACCOUNT A ;

insert into Business_Account (Account_id, BUsiness_name, ADDRESS_LINE_1 , city, State, Zip, Country, About, Phone_number) values (1000, 'Mosciski Inc', '66390 1st Court', 'Abaeté', 'Minas Gerais', '35620000', 'Brazil', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 6186485831);
insert into Business_Account (Account_id, BUsiness_name, ADDRESS_LINE_1 , city, State, Zip, Country, About, Phone_number) values (1005, 'Douglas, Ankunding and Grimes', '778 Hoepker Junction', 'Randudongkal', 'Central Java', 5235, 'Indonesia', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3061030055);
SELECT * from BUSINESS_ACCOUNT BA ;

insert into Personal_Account (Personal_user_id, Username, First_name, Last_name, Short_bio) values (1003, 'mchurching0', 'Mord', 'Churching', 'Hebephrenia-subchronic');
insert into Personal_Account (Personal_user_id, Username, First_name, Last_name, Short_bio) values (1005, 'dbeazey1', 'Dniren', 'Beazey', 'Fail mech induct-antepar');
SELECT * from PERSONAL_ACCOUNT PA ;


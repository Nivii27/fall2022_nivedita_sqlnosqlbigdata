--schema creation
create schema mot;
use mot;
create table user (
	id integer PRIMARY KEY,
	name varchar(40),
	city varchar(20),
	age smallint,
	gender varchar(10)
);

--insert statements
UPSERT INTO user VALUES (1,'Melissa Noel','Wolfsberg',19,'Male');
UPSERT INTO user VALUES (2, 'Casey Ewing','Essen',39,'Female');
UPSERT INTO user VALUES (3,'Brenda Morgan','Smoky Lake',37,'Female');
UPSERT INTO user VALUES (4,'Norman Phillips','Maidenhead',26,'Female');
UPSERT INTO user VALUES (5,'Octavius Cobb','Etobicoke',35,'Female');
UPSERT INTO user VALUES (6,'Kyle Mathis','Temuco',38,'Male');

select * from user;
select * from user where gender= 'Female';
select * from user where id = 22;

--aggregation query
select gender, avg(age) from user group by gender;

